﻿using Lab5.PageObjects;
using OpenQA.Selenium;

namespace Lab5.PageObjects
{
    public class VeterinariansPageObject : BasePageObject
    {
        private By VeterinariansPageHeader = By.CssSelector("h2");

        public VeterinariansPageObject(IWebDriver driver) : base(driver) { }

        public string GetVeterinariansPageHeaderText()
        {
            return driver.FindElement(VeterinariansPageHeader).Text;
        }
    }
}
