﻿using Lab5.PageObjects;
using OpenQA.Selenium;

namespace Lab5.PageObjects
{
    public class OwnersPageObject : BasePageObject
    {
        private By OwnersPageHeader = By.CssSelector("h2");

        public OwnersPageObject(IWebDriver driver) : base(driver) { }

        public string GetOwnersPageHeaderText()
        {
            return driver.FindElement(OwnersPageHeader).Text;
        }
    }
}
