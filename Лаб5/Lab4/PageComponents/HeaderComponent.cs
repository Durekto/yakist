﻿using OpenQA.Selenium;

namespace Lab5.PageComponents
{
    public class HeaderComponent
    {
        private IWebDriver driver;

        public HeaderComponent(IWebDriver driver) => this.driver = driver;

        private IWebElement OwnersDropdown() => driver.FindElement(By.CssSelector(".ownerTab"));
        private IWebElement OwnersAllItem() => driver.FindElement(By.CssSelector(".open li:nth-child(1) > a"));

        private IWebElement PetTypesListItem() => driver.FindElement(By.CssSelector("li:nth-child(4) > a"));

        private IWebElement SpecialtiesMenuItem() => driver.FindElement(By.CssSelector("li:nth-child(5) span:nth-child(2)"));
       
        private IWebElement VeterinariansListItem() => driver.FindElement(By.CssSelector(".vetsTab"));
        private IWebElement VeterinariansAllItem() => driver.FindElement(By.CssSelector(".open li:nth-child(1) > a"));

        public void OpenOwnersPage()
        {
            OwnersDropdown().Click();
            OwnersAllItem().Click();
        }

        public void OpenPetTypesPage()
        {
            PetTypesListItem().Click();
        }

        public void OpenSpecialtiesPage()
        {
            SpecialtiesMenuItem().Click();
        }

        public void OpenVeterinariansPage()
        {
            VeterinariansListItem().Click();
            VeterinariansAllItem().Click();
        }
    }
}
