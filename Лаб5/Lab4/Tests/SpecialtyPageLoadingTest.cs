using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageFactories;
using Lab5.PageComponents;

namespace Lab5
{
    [TestFixture]
    public class SpecialtyPageLoadingTest : BaseTest
    {
        [Test]
        public void SpecialtyPageLoading()
        {
            HeaderComponent header = Components.Header;
            SpecialtiesPageObject specialtiesPage = Pages.SpecialtiesPage;
            header.OpenSpecialtiesPage();
            Helper.Wait();
            Assert.That(specialtiesPage.GetSpecialtiesPageHeaderText(), Is.EqualTo("Specialties"));
        }
    }
}