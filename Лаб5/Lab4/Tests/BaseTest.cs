﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace Lab5.Tests
{
    public class BaseTest
    {
        public static IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://20.50.171.10:8080/");
            driver.Manage().Window.Size = new System.Drawing.Size(764, 674);
        }

        [TearDown]
        protected void TearDown()
        {
            driver.Quit();
        }
    }
}
