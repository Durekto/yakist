using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageFactories;
using Lab5.PageComponents;

namespace Lab5
{
    [TestFixture]
    public class SpecialtyDeletingTest : BaseTest
    {
        [Test]
        public void SpecialtyDeleting()
        {
            HeaderComponent header = Components.Header;
            SpecialtiesPageObject specialtiesPage = Pages.SpecialtiesPage;
            header.OpenSpecialtiesPage();
            Helper.Wait();
            int previousCount = specialtiesPage.DeleteSpecialty();
            Helper.Wait();
            Assert.That(specialtiesPage.GetSpecialtiesCount(), Is.EqualTo(previousCount - 1));
        }
    }
}