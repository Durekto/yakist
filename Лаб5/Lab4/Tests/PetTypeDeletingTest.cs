using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageComponents;
using Lab5.PageFactories;

namespace Lab5
{
    [TestFixture]
    public class PetTypeDeletingTest : BaseTest
    {
        [Test]
        public void PetTypeDeleting()
        {
            HeaderComponent header = Components.Header;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            header.OpenPetTypesPage();
            Helper.Wait();
            int previousCount = petTypesPage.DeletePetType();
            Helper.Wait();
            Assert.That(petTypesPage.GetPetTypesCount(), Is.EqualTo(previousCount - 1));
        }
    }
}