using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageFactories;
using Lab5.PageComponents;

namespace Lab5
{
    [TestFixture]
    public class OwnersPageLoadingTest : BaseTest
    {
        [Test]
        public void OwnersPageLoading()
        {
            HeaderComponent header = Components.Header;
            OwnersPageObject ownersPage = Pages.OwnersPage;
            header.OpenOwnersPage();
            Helper.Wait();
            Assert.That(ownersPage.GetOwnersPageHeaderText(), Is.EqualTo("Owners"));
        }
    }
}