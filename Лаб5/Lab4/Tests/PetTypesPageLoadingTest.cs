using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageFactories;
using Lab5.PageComponents;

namespace Lab5
{
    [TestFixture]
    public class PetTypesPageLoadingTest : BaseTest
    {
        [Test]
        public void PetTypesPageLoading()
        {
            HeaderComponent header = Components.Header;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            header.OpenPetTypesPage();
            Helper.Wait();
            Assert.That(petTypesPage.GetPetTypesPageHeaderText(), Is.EqualTo("Pet Types"));
        }
    }
}