using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageFactories;

namespace Lab5
{
    [TestFixture]
    public class HomePageLoadingTest : BaseTest
    {
        [Test]
        public void HomePageLoading()
        {
            HomePageObject homePage = Pages.HomePage;
            Helper.Wait();
            Assert.That(homePage.GetHomePageHeaderText(), Is.EqualTo("Welcome to Petclinic"));
        }
    }
}