using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageFactories;
using Lab5.PageComponents;

namespace Lab5
{
    [TestFixture]
    public class PetTypeCreatingTest : BaseTest
    {
        [Test]
        public void PetTypeCreation()
        {
            HeaderComponent header = Components.Header;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            header.OpenPetTypesPage();
            Helper.Wait();
            petTypesPage.AddPetType("Cat");
            Helper.Wait();
            Assert.That(petTypesPage.GetPetTypesLastName(), Is.EqualTo("Cat"));
        }
    }
}