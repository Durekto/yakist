using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageFactories;
using Lab5.PageComponents;

namespace Lab5
{
    [TestFixture]
    public class SpecialtyCreatingTest : BaseTest
    {
        [Test]
        public void SpecialtyCreating()
        {
            HeaderComponent header = Components.Header;
            SpecialtiesPageObject specialtiesPage = Pages.SpecialtiesPage;
            header.OpenSpecialtiesPage();
            Helper.Wait();
            specialtiesPage.AddSpecialty("GP");
            Helper.Wait();
            Assert.That(specialtiesPage.GetSpecialtiesLastName(), Is.EqualTo("GP"));
        }
    }
}