using NUnit.Framework;
using Lab5.Tests;
using Lab5.PageObjects;
using Lab5.PageFactories;
using Lab5.PageComponents;

namespace Lab5
{
    [TestFixture]
    public class VeterinariansPageLoadingTest : BaseTest
    {
        [Test]
        public void VeterinariansPageLoading()
        {
            HeaderComponent header = Components.Header;
            VeterinariansPageObject veterinariansPage = Pages.VeterinariansPage;
            header.OpenVeterinariansPage();
            Helper.Wait();
            Assert.That(veterinariansPage.GetVeterinariansPageHeaderText(), Is.EqualTo("Veterinarians"));
        }
    }
}