﻿using Lab5.PageObjects;
using Lab5.Tests;

namespace Lab5.PageFactories
{
    public static class Pages
    {
        public static HomePageObject HomePage => new HomePageObject(BaseTest.driver);
        public static OwnersPageObject OwnersPage => new OwnersPageObject(BaseTest.driver);
        public static PetTypesPageObject PetTypesPage => new PetTypesPageObject(BaseTest.driver);
        public static SpecialtiesPageObject SpecialtiesPage => new SpecialtiesPageObject(BaseTest.driver);
        public static VeterinariansPageObject VeterinariansPage => new VeterinariansPageObject(BaseTest.driver);
    }
}
