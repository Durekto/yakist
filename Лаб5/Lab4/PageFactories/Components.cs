﻿using Lab5.PageComponents;
using Lab5.Tests;

namespace Lab5.PageFactories
{
    public static class Components
    {
        public static HeaderComponent Header => new HeaderComponent(BaseTest.driver);
    }
}
