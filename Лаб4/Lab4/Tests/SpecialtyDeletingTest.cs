using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class SpecialtyDeletingTest : BaseTest
    {
        [Test]
        public void SpecialtyDeleting()
        {
            SpecialtiesPageObject specialtiesPage = new SpecialtiesPageObject(driver);
            specialtiesPage.OpenSpecialtiesPage();
            Wait();
            int previousCount = specialtiesPage.DeleteSpecialty();
            Wait();
            Assert.That(specialtiesPage.GetSpecialtiesCount(), Is.EqualTo(previousCount - 1));
        }
    }
}