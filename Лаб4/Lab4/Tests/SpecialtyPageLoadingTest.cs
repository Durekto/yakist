using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class SpecialtyPageLoadingTest : BaseTest
    {
        [Test]
        public void SpecialtyPageLoading()
        {
            SpecialtiesPageObject specialtiesPage = new SpecialtiesPageObject(driver);
            specialtiesPage.OpenSpecialtiesPage();
            Wait();
            Assert.That(specialtiesPage.GetSpecialtiesPageHeaderText(), Is.EqualTo("Specialties"));
        }
    }
}