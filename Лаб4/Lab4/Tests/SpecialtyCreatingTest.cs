using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class SpecialtyCreatingTest : BaseTest
    {
        [Test]
        public void SpecialtyCreating()
        {
            SpecialtiesPageObject specialtiesPage = new SpecialtiesPageObject(driver);
            specialtiesPage.OpenSpecialtiesPage();
            Wait();
            specialtiesPage.AddSpecialty("GP");
            Wait();
            Assert.That(specialtiesPage.GetSpecialtiesLastName(), Is.EqualTo("GP"));
        }
    }
}