using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class OwnersPageLoadingTest : BaseTest
    {
        [Test]
        public void OwnersPageLoading()
        {
            OwnersPageObject ownersPage = new OwnersPageObject(driver);
            ownersPage.OpenOwnersPage();
            Wait();
            Assert.That(ownersPage.GetOwnersPageHeaderText(), Is.EqualTo("Owners"));
        }
    }
}