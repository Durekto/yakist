using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class PetTypesPageLoadingTest : BaseTest
    {
        [Test]
        public void PetTypesPageLoading()
        {
            PetTypesPageObject petTypesPage = new PetTypesPageObject(driver);
            petTypesPage.OpenPetTypesPage();
            Wait();
            Assert.That(petTypesPage.GetPetTypesPageHeaderText(), Is.EqualTo("Pet Types"));
        }
    }
}