using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class PetTypeDeletingTest : BaseTest
    {
        [Test]
        public void PetTypeDeleting()
        {
            PetTypesPageObject petTypesPage = new PetTypesPageObject(driver);
            petTypesPage.OpenPetTypesPage();
            Wait();
            int previousCount = petTypesPage.DeletePetType();
            Wait();
            Assert.That(petTypesPage.GetPetTypesCount(), Is.EqualTo(previousCount - 1));
        }
    }
}