using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class PetTypeEditingTest : BaseTest
    {
        [Test]
        public void PetTypeEditing()
        {
            PetTypesPageObject petTypesPage = new PetTypesPageObject(driver);
            petTypesPage.OpenPetTypesPage();
            Wait();
            petTypesPage.EditPetType("EditedName");
            Wait();
            Assert.That(petTypesPage.GetPetTypesLastName(), Is.EqualTo("EditedName"));
        }
    }
}