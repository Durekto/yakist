using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class VeterinariansPageLoadingTest : BaseTest
    {
        [Test]
        public void VeterinariansPageLoading()
        {
            VeterinariansPageObject veterinariansPage = new VeterinariansPageObject(driver);
            veterinariansPage.OpenVeterinariansPage();
            Wait();
            Assert.That(veterinariansPage.GetVeterinariansPageHeaderText(), Is.EqualTo("Veterinarians"));
        }
    }
}