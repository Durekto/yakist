using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class PetTypeCreatingTest : BaseTest
    {
        [Test]
        public void PetTypeCreation()
        {
            PetTypesPageObject petTypesPage = new PetTypesPageObject(driver);
            petTypesPage.OpenPetTypesPage();
            Wait();
            petTypesPage.AddPetType("Cat");
            Wait();
            Assert.That(petTypesPage.GetPetTypesLastName(), Is.EqualTo("Cat"));
        }
    }
}