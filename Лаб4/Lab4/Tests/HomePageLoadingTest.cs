using NUnit.Framework;
using Lab4.Tests;
using Lab4.PageObjects;

namespace Lab4
{
    [TestFixture]
    public class HomePageLoadingTest : BaseTest
    {
        [Test]
        public void HomePageLoading()
        {
            HomePageObject homePage = new HomePageObject(driver);
            Wait();
            Assert.That(homePage.GetHomePageHeaderText(), Is.EqualTo("Welcome to Petclinic"));
        }
    }
}