﻿using Lab4.PageObjects;
using OpenQA.Selenium;

namespace Lab4.PageObjects
{
    public class VeterinariansPageObject : BasePageObject
    {
        private By VeterinariansListItem = By.CssSelector(".vetsTab");
        private By VeterinariansAllItem = By.CssSelector(".open li:nth-child(1) > a");
        private By VeterinariansPageHeader = By.CssSelector("h2");

        public VeterinariansPageObject(IWebDriver driver) : base(driver) { }

        public void OpenVeterinariansPage()
        {
            driver.FindElement(VeterinariansListItem).Click();
            driver.FindElement(VeterinariansAllItem).Click();
        }

        public string GetVeterinariansPageHeaderText()
        {
            return driver.FindElement(VeterinariansPageHeader).Text;
        }
    }
}
