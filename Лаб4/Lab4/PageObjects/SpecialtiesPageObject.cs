﻿using Lab4.PageObjects;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.PageObjects
{
    public class SpecialtiesPageObject : BasePageObject
    {
        private By SpecialtiesMenuItem = By.CssSelector("li:nth-child(5) span:nth-child(2)");
        private By SpecialtiesPageHeader = By.CssSelector("h2");
        private By SpecialtiesAddButton = By.CssSelector(".addSpecialty");
        private By SpecialtyName = By.Id("name");
        private By SpecialtySaveButton = By.CssSelector(".btn:nth-child(3)");
        private By SpecialtiesLastName = By.CssSelector("tbody > tr:last-of-type > td > input");
        private By SpecialtiesItem = By.CssSelector("tbody > tr");
        private By SpecialtiesLastDeleteButton = By.CssSelector("tbody > tr:last-of-type > td > button.deleteSpecialty");

        public SpecialtiesPageObject(IWebDriver driver) : base(driver) { }

        public void OpenSpecialtiesPage()
        {
            driver.FindElement(SpecialtiesMenuItem).Click();
        }

        public string GetSpecialtiesPageHeaderText()
        {
            return driver.FindElement(SpecialtiesPageHeader).Text;
        }

        public void AddSpecialty(string name)
        {
            driver.FindElement(SpecialtiesAddButton).Click();
            driver.FindElement(SpecialtyName).Click();
            driver.FindElement(SpecialtyName).SendKeys(name);
            driver.FindElement(SpecialtySaveButton).Click();
        }

        public string GetSpecialtiesLastName()
        {
            return driver.FindElement(SpecialtiesLastName).GetAttribute("value");
        }

        public int GetSpecialtiesCount()
        {
            return driver.FindElements(SpecialtiesItem).Count;
        }

        public int DeleteSpecialty()
        {
            int count = GetSpecialtiesCount();
            driver.FindElement(SpecialtiesLastDeleteButton).Click();
            return count;
        }
    }
}
