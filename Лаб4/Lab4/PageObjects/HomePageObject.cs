﻿using OpenQA.Selenium;

namespace Lab4.PageObjects
{
    public class HomePageObject : BasePageObject
    {
        private By HomePageHeader = By.CssSelector("h1");

        public HomePageObject(IWebDriver driver) : base(driver) {}

        public string GetHomePageHeaderText()
        {
            return driver.FindElement(HomePageHeader).Text;
        }
    }
}
