﻿using Lab4.PageObjects;
using OpenQA.Selenium;

namespace Lab4.PageObjects
{
    public class PetTypesPageObject : BasePageObject
    {
        private By PetTypesListItem = By.CssSelector("li:nth-child(4) > a");
        private By PetTypesPageHeader = By.CssSelector("h2");
        private By PetTypeAddButton = By.CssSelector(".addPet");
        private By PetTypeName = By.Id("name");
        private By PetTypeSaveButton = By.CssSelector(".saveType");
        private By PetTypesLastName = By.CssSelector("tbody > tr:last-of-type > td > input");
        private By PetTypesLastEditButton = By.CssSelector("tbody > tr:last-of-type > td > button.editPet");
        private By PetTypesUpdateButton = By.CssSelector(".updatePetType");
        private By PetTypesLastDeleteButton = By.CssSelector("tbody > tr:last-of-type > td > button.deletePet");
        private By PetTypesItem = By.CssSelector("tbody > tr");

        public PetTypesPageObject(IWebDriver driver) : base(driver) { }

        public void OpenPetTypesPage()
        {
            driver.FindElement(PetTypesListItem).Click();
        }

        public string GetPetTypesPageHeaderText()
        {
            return driver.FindElement(PetTypesPageHeader).Text;
        }

        public void AddPetType(string name)
        {
            driver.FindElement(PetTypeAddButton).Click();
            driver.FindElement(PetTypeName).Click();
            driver.FindElement(PetTypeName).SendKeys(name);
            driver.FindElement(PetTypeSaveButton).Click();
        }

        public string GetPetTypesLastName()
        {
            return driver.FindElement(PetTypesLastName).GetAttribute("value");
        }

        public void EditPetType(string name)
        {
            driver.FindElement(PetTypesLastEditButton).Click();
            driver.FindElement(PetTypeName).Click();
            driver.FindElement(PetTypeName).Clear();
            driver.FindElement(PetTypeName).SendKeys(name);
            driver.FindElement(PetTypesUpdateButton).Click();
        }

        public int GetPetTypesCount()
        {
            return driver.FindElements(PetTypesItem).Count;
        }

        public int DeletePetType()
        {
            int count = GetPetTypesCount();
            driver.FindElement(PetTypesLastDeleteButton).Click();
            return count;
        }
    }
}
