﻿using Lab4.PageObjects;
using OpenQA.Selenium;

namespace Lab4.PageObjects
{
    public class OwnersPageObject : BasePageObject
    {
        private By OwnersPageHeader = By.CssSelector("h2");
        private By OwnersDropdown = By.CssSelector(".ownerTab");
        private By OwnersAllItem = By.CssSelector(".open li:nth-child(1) > a");

        public OwnersPageObject(IWebDriver driver) : base(driver) { }

        public string GetOwnersPageHeaderText()
        {
            return driver.FindElement(OwnersPageHeader).Text;
        }

        public void OpenOwnersPage()
        {
            driver.FindElement(OwnersDropdown).Click();
            driver.FindElement(OwnersAllItem).Click();
        }
    }
}
