﻿using Lab7.PageComponents;
using Lab7.Tests;

namespace Lab7.PageFactories
{
    public static class Components
    {
        public static HeaderComponent Header => new HeaderComponent(BaseTest.driver);
    }
}
