﻿using Lab7.PageObjects;
using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lab7.PageObjects
{
    public class OwnersPageObject : BasePageObject
    {
        private By OwnersPageHeader = By.CssSelector("h2");

        public OwnersPageObject(IWebDriver driver) : base(driver) { }

        [AllureStep("Get owners page header text")]
        public string GetOwnersPageHeaderText()
        {
            return driver.FindElement(OwnersPageHeader).Text;
        }
    }
}
