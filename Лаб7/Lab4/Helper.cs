﻿using OpenQA.Selenium;
using System.Threading;

namespace Lab7
{
    class Helper
    {
        public static void ClickAndSendKeys(IWebElement webElement, string keys)
        {
            webElement.Click();
            webElement.SendKeys(keys);
        }

        public static void ClickAndClearAndSendKeys(IWebElement webElement, string keys)
        {
            webElement.Click();
            webElement.Clear();
            webElement.SendKeys(keys);
        }

        public static void Wait(int delay = 1000)
        {
            Thread.Sleep(delay);
        }
    }
}
