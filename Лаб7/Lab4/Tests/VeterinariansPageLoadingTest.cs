using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageFactories;
using Lab7.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class VeterinariansPageLoadingTest : BaseTest
    {
        [Test, Description("This test checks that veterinarians page could be loaded successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Veterinarian")]
        public void VeterinariansPageLoading()
        {
            HeaderComponent header = Components.Header;
            VeterinariansPageObject veterinariansPage = Pages.VeterinariansPage;
            header.OpenVeterinariansPage();
            Helper.Wait();
            Assert.That(veterinariansPage.GetVeterinariansPageHeaderText(), Is.EqualTo("Veterinarians"));
        }
    }
}