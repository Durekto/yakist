using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageFactories;
using Lab7.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class PetTypeEditingTest : BaseTest
    {
        static object[] TestData = { "Name1", "Name2", "Name3", "Name4", "Name5" };

        [Test, Description("This test checks that pet type could be edited successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Pet type")]
        [TestCaseSource(nameof(TestData))]
        public void PetTypeEditing(string name)
        {
            HeaderComponent header = Components.Header;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            header.OpenPetTypesPage();
            Helper.Wait();
            petTypesPage.EditPetType(name);
            Helper.Wait();
            Assert.That(petTypesPage.GetPetTypesLastName(), Is.EqualTo(name));
        }
    }
}