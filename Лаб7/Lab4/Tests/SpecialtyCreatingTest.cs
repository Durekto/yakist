using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageFactories;
using Lab7.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class SpecialtyCreatingTest : BaseTest
    {
        [Test, Description("This test checks that specialty could be created successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Specialty")]
        public void SpecialtyCreating([Values("GP", "Surgeon", "Psychiatrist", "Laryngologist")] string name)
        {
            HeaderComponent header = Components.Header;
            SpecialtiesPageObject specialtiesPage = Pages.SpecialtiesPage;
            header.OpenSpecialtiesPage();
            Helper.Wait();
            specialtiesPage.AddSpecialty(name);
            Helper.Wait();
            Assert.That(specialtiesPage.GetSpecialtiesLastName(), Is.EqualTo(name));
        }
    }
}