using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageFactories;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class HomePageLoadingTest : BaseTest
    {
        [Test, Description("This test checks that home page could be loaded successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Home")]
        public void HomePageLoading()
        {
            HomePageObject homePage = Pages.HomePage;
            Helper.Wait();
            Assert.That(homePage.GetHomePageHeaderText(), Is.EqualTo("Welcome to Petclinic"));
        }
    }
}