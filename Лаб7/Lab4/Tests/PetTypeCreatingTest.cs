using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageFactories;
using Lab7.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class PetTypeCreatingTest : BaseTest
    {
        [Test, Description("This test checks that pet type could be created successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [TestCase("Cat")]
        [TestCase("Dog")]
        [TestCase("Snake")]
        [TestCase("Pig")]
        [TestCase("Lion")]
        [AllureSuite("Pet type")]
        public void PetTypeCreation(string name)
        {
            HeaderComponent header = Components.Header;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            header.OpenPetTypesPage();
            Helper.Wait();
            petTypesPage.AddPetType(name);
            Helper.Wait();
            Assert.That(petTypesPage.GetPetTypesLastName(), Is.EqualTo(name));
        }
    }
}