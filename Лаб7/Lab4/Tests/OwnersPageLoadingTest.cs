using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageFactories;
using Lab7.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class OwnersPageLoadingTest : BaseTest
    {
        [Test, Description("This test checks that owners page could be loaded successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Owner")]
        public void OwnersPageLoading()
        {
            HeaderComponent header = Components.Header;
            OwnersPageObject ownersPage = Pages.OwnersPage;
            header.OpenOwnersPage();
            Helper.Wait();
            Assert.That(ownersPage.GetOwnersPageHeaderText(), Is.EqualTo("Owners"));
        }
    }
}