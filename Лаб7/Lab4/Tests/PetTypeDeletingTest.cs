using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageComponents;
using Lab7.PageFactories;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class PetTypeDeletingTest : BaseTest
    {
        [Test, Description("This test checks that pet type could be deleted successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Pet type")]
        public void PetTypeDeleting()
        {
            HeaderComponent header = Components.Header;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            header.OpenPetTypesPage();
            Helper.Wait();
            int previousCount = petTypesPage.DeletePetType();
            Helper.Wait();
            Assert.That(petTypesPage.GetPetTypesCount(), Is.EqualTo(previousCount - 1));
        }
    }
}