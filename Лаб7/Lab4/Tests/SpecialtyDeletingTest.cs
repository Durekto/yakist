using NUnit.Framework;
using Lab7.Tests;
using Lab7.PageObjects;
using Lab7.PageFactories;
using Lab7.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class SpecialtyDeletingTest : BaseTest
    {
        [Test, Description("This test checks that specialty could be deleted successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Specialty")]
        public void SpecialtyDeleting()
        {
            HeaderComponent header = Components.Header;
            SpecialtiesPageObject specialtiesPage = Pages.SpecialtiesPage;
            header.OpenSpecialtiesPage();
            Helper.Wait();
            int previousCount = specialtiesPage.DeleteSpecialty();
            Helper.Wait();
            Assert.That(specialtiesPage.GetSpecialtiesCount(), Is.EqualTo(previousCount - 1));
        }
    }
}