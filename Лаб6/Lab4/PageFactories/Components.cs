﻿using Lab6.PageComponents;
using Lab6.Tests;

namespace Lab6.PageFactories
{
    public static class Components
    {
        public static HeaderComponent Header => new HeaderComponent(BaseTest.driver);
    }
}
