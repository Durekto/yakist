using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageFactories;
using Lab6.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class SpecialtyPageLoadingTest : BaseTest
    {
        [Test, Description("This test checks that specialties page could be loaded successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Specialty")]
        public void SpecialtyPageLoading()
        {
            HeaderComponent header = Components.Header;
            SpecialtiesPageObject specialtiesPage = Pages.SpecialtiesPage;
            header.OpenSpecialtiesPage();
            Helper.Wait();
            Assert.That(specialtiesPage.GetSpecialtiesPageHeaderText(), Is.EqualTo("Specialties"));
        }
    }
}