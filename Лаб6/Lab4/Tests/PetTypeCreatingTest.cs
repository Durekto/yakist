using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageFactories;
using Lab6.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class PetTypeCreatingTest : BaseTest
    {
        [Test, Description("This test checks that pet type could be created successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Pet type")]
        public void PetTypeCreation()
        {
            HeaderComponent header = Components.Header;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            header.OpenPetTypesPage();
            Helper.Wait();
            petTypesPage.AddPetType("Cat");
            Helper.Wait();
            Assert.That(petTypesPage.GetPetTypesLastName(), Is.EqualTo("Cat"));
        }
    }
}