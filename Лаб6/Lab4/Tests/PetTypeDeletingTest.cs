using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageComponents;
using Lab6.PageFactories;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class PetTypeDeletingTest : BaseTest
    {
        [Test, Description("This test checks that pet type could be deleted successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Pet type")]
        public void PetTypeDeleting()
        {
            HeaderComponent header = Components.Header;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            header.OpenPetTypesPage();
            Helper.Wait();
            int previousCount = petTypesPage.DeletePetType();
            Helper.Wait();
            Assert.That(petTypesPage.GetPetTypesCount(), Is.EqualTo(previousCount - 1));
        }
    }
}