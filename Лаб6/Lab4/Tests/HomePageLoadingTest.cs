using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageFactories;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class HomePageLoadingTest : BaseTest
    {
        [Test, Description("This test checks that home page could be loaded successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Home")]
        public void HomePageLoading()
        {
            HomePageObject homePage = Pages.HomePage;
            Helper.Wait();
            Assert.That(homePage.GetHomePageHeaderText(), Is.EqualTo("Welcome to Petclinic"));
        }
    }
}