using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageFactories;
using Lab6.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class OwnersPageLoadingTest : BaseTest
    {
        [Test, Description("This test checks that owners page could be loaded successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Owner")]
        public void OwnersPageLoading()
        {
            HeaderComponent header = Components.Header;
            OwnersPageObject ownersPage = Pages.OwnersPage;
            header.OpenOwnersPage();
            Helper.Wait();
            Assert.That(ownersPage.GetOwnersPageHeaderText(), Is.EqualTo("Owners"));
        }
    }
}