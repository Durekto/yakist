using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageFactories;
using Lab6.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class SpecialtyDeletingTest : BaseTest
    {
        [Test, Description("This test checks that specialty could be deleted successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Specialty")]
        public void SpecialtyDeleting()
        {
            HeaderComponent header = Components.Header;
            SpecialtiesPageObject specialtiesPage = Pages.SpecialtiesPage;
            header.OpenSpecialtiesPage();
            Helper.Wait();
            int previousCount = specialtiesPage.DeleteSpecialty();
            Helper.Wait();
            Assert.That(specialtiesPage.GetSpecialtiesCount(), Is.EqualTo(previousCount - 1));
        }
    }
}