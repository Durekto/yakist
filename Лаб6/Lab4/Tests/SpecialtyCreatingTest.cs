using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageFactories;
using Lab6.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class SpecialtyCreatingTest : BaseTest
    {
        [Test, Description("This test checks that specialty could be created successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Specialty")]
        public void SpecialtyCreating()
        {
            HeaderComponent header = Components.Header;
            SpecialtiesPageObject specialtiesPage = Pages.SpecialtiesPage;
            header.OpenSpecialtiesPage();
            Helper.Wait();
            specialtiesPage.AddSpecialty("GP");
            Helper.Wait();
            Assert.That(specialtiesPage.GetSpecialtiesLastName(), Is.EqualTo("GP"));
        }
    }
}