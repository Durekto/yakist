using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageFactories;
using Lab6.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class PetTypeEditingTest : BaseTest
    {
        [Test, Description("This test checks that pet type could be edited successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Pet type")]
        public void PetTypeEditing()
        {
            HeaderComponent header = Components.Header;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            header.OpenPetTypesPage();
            Helper.Wait();
            petTypesPage.EditPetType("EditedName");
            Helper.Wait();
            Assert.That(petTypesPage.GetPetTypesLastName(), Is.EqualTo("EditedName"));
        }
    }
}