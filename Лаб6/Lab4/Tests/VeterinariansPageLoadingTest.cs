using NUnit.Framework;
using Lab6.Tests;
using Lab6.PageObjects;
using Lab6.PageFactories;
using Lab6.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lab6
{
    [TestFixture]
    [AllureNUnit]
    public class VeterinariansPageLoadingTest : BaseTest
    {
        [Test, Description("This test checks that veterinarians page could be loaded successfully")]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureSuite("Veterinarian")]
        public void VeterinariansPageLoading()
        {
            HeaderComponent header = Components.Header;
            VeterinariansPageObject veterinariansPage = Pages.VeterinariansPage;
            header.OpenVeterinariansPage();
            Helper.Wait();
            Assert.That(veterinariansPage.GetVeterinariansPageHeaderText(), Is.EqualTo("Veterinarians"));
        }
    }
}