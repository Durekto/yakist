﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lab6.PageObjects
{
    public class HomePageObject : BasePageObject
    {
        private By HomePageHeader = By.CssSelector("h1");

        public HomePageObject(IWebDriver driver) : base(driver) {}

        [AllureStep("Get home page header text")]
        public string GetHomePageHeaderText()
        {
            return driver.FindElement(HomePageHeader).Text;
        }
    }
}
