﻿using Lab6.PageObjects;
using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lab6.PageObjects
{
    public class PetTypesPageObject : BasePageObject
    {
        private By PetTypesPageHeader = By.CssSelector("h2");
        private By PetTypeAddButton = By.CssSelector(".addPet");
        private By PetTypeName = By.Id("name");
        private By PetTypeSaveButton = By.CssSelector(".saveType");
        private By PetTypesLastName = By.CssSelector("tbody > tr:last-of-type > td > input");
        private By PetTypesLastEditButton = By.CssSelector("tbody > tr:last-of-type > td > button.editPet");
        private By PetTypesUpdateButton = By.CssSelector(".updatePetType");
        private By PetTypesLastDeleteButton = By.CssSelector("tbody > tr:last-of-type > td > button.deletePet");
        private By PetTypesItem = By.CssSelector("tbody > tr");

        public PetTypesPageObject(IWebDriver driver) : base(driver) { }

        [AllureStep("Get pet types page header text")]
        public string GetPetTypesPageHeaderText()
        {
            return driver.FindElement(PetTypesPageHeader).Text;
        }

        [AllureStep("Open add form; enter values; press Save button")]
        public void AddPetType(string name)
        {
            driver.FindElement(PetTypeAddButton).Click();
            Helper.ClickAndSendKeys(driver.FindElement(PetTypeName), name);
            driver.FindElement(PetTypeSaveButton).Click();
        }

        [AllureStep("Get last item name")]
        public string GetPetTypesLastName()
        {
            return driver.FindElement(PetTypesLastName).GetAttribute("value");
        }

        [AllureStep("Open edit form; enter values; press Update button")]
        public void EditPetType(string name)
        {
            driver.FindElement(PetTypesLastEditButton).Click();
            Helper.ClickAndClearAndSendKeys(driver.FindElement(PetTypeName), name);
            driver.FindElement(PetTypesUpdateButton).Click();
        }

        [AllureStep("Get pet types count")]
        public int GetPetTypesCount()
        {
            return driver.FindElements(PetTypesItem).Count;
        }

        [AllureStep("Press Delete button")]
        public int DeletePetType()
        {
            int count = GetPetTypesCount();
            driver.FindElement(PetTypesLastDeleteButton).Click();
            return count;
        }
    }
}
