﻿using Lab6.PageObjects;
using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lab6.PageObjects
{
    public class VeterinariansPageObject : BasePageObject
    {
        private By VeterinariansPageHeader = By.CssSelector("h2");

        public VeterinariansPageObject(IWebDriver driver) : base(driver) { }

        [AllureStep("Get veterinarians page header text")]
        public string GetVeterinariansPageHeaderText()
        {
            return driver.FindElement(VeterinariansPageHeader).Text;
        }
    }
}
